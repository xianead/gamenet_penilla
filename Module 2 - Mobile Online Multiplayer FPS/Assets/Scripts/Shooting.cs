using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Random = UnityEngine.Random;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;

public class Shooting : MonoBehaviourPunCallbacks {

    public Camera cam; 

    private Animator anim;
    
    public GameObject hitEffectPrefab;

    private int score;

    public Text killFeed;

    [Header("HP Related Variables")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private static readonly int IsDead = Animator.StringToHash("IsDead");

    // Start is called before the first frame update
    void Start() {
        health = startHealth;
        score = 0;
        UpdateHealthBar();

        anim = GetComponent<Animator>();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            DisconnectPlayer();
        }
        if (Input.GetKeyDown(KeyCode.A)) {
            GameManager.Instance.test();
        }
    }

    public void Fire() {
        RaycastHit hit;
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200)) {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("HitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, this.transform.gameObject.GetPhotonView().ViewID);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int senderView, PhotonMessageInfo info) {
        this.health -= 25;
        UpdateHealthBar();

        if (this.health <= 0) {
            var killerName = info.Sender.NickName;
            var victimName = info.photonView.Owner.NickName;

            Die(killerName, victimName);

            PhotonView.Find(senderView).transform.gameObject.GetComponent<Shooting>().AddScore();
        }
    }

    public void TestDmg() {
        this.health -= 50;
        UpdateHealthBar();
        Debug.Log("Took 50 dmg");
        
        if (this.health <= 0) {
            Die("xian", "pogi");
        }
    }

    private void UpdateHealthBar() {
        this.healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void HitEffects(Vector3 pos) {
        GameObject hitVfx = Instantiate(hitEffectPrefab, pos, Quaternion.identity);
        Destroy(hitVfx, 0.2f);
    }

    public void Die(string killerName, string victimName) {
        this.photonView.RPC("AddToKillFeed", RpcTarget.All, killerName, victimName);

        if (photonView.IsMine) {
            anim.SetBool(IsDead, true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown() {
        var respawnText = GameObject.Find("RespawnText");
        var respawnTime = 5.0f;

        while (respawnTime > 0) {
            yield return new WaitForSeconds(1.0f);

            respawnTime--; 
            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You have been killed. Respawning in " + respawnTime.ToString(".00");
        }

        if (!GameManager.Instance.HasWon){
            anim.SetBool(IsDead, false);
            respawnText.GetComponent<Text>().text = ""; 

            this.transform.position = GameManager.Instance.GetRandomSP();
            transform.GetComponent<PlayerMovementController>().enabled = true;

            photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
        }
        else if (GameManager.Instance.HasWon){
            StartCoroutine(DisconnectAndLoad());
        }
    }

    [PunRPC] 
    public void RegainHealth() {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void AddToKillFeed(string killerName, string victimName) {
        StartCoroutine(DisplayKillFeed(killerName,victimName));
    }

    IEnumerator DisplayKillFeed(string killerName, string victimName) {
        var killText = GameObject.Find("KillFeedText");

        var msgTime = 0f;
        var msgMaxTime = 3.0f;

        while (msgMaxTime > msgTime) {
            yield return new WaitForSeconds(1.0f);
            killText.GetComponent<Text>().text = killerName + " pwned " + victimName;

            msgMaxTime--;
        }

        killText.GetComponent<Text>().text = "";
    }

    public void AddScore() {
        score += 5;

        if (score >= 10) {
            Debug.Log("You win");
            GameManager.Instance.HasWon = true;
            this.photonView.RPC("DisplayWinner", RpcTarget.AllBuffered, this.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void DisplayWinner(string name) {
        StartCoroutine(AddWinnerToFeed(name));
    }

    IEnumerator AddWinnerToFeed(string name) {
        var winnerText = GameObject.Find("WinnerText");

        var msgTime = 0f;
        var msgMaxTime = 4.0f;

        while (msgMaxTime > msgTime) {
            yield return new WaitForSeconds(1.0f);

            transform.GetComponent<PlayerMovementController>().enabled = false;
            winnerText.GetComponent<Text>().text = "The winner is: " + name + "!";

            msgMaxTime--;
        }
        this.photonView.RPC("DisconnectPlayer", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void DisconnectPlayer() {
        StartCoroutine(DisconnectAndLoad());
    }

    IEnumerator DisconnectAndLoad() {
        PhotonNetwork.LeaveRoom();

        while (PhotonNetwork.InRoom) {
            yield return null;
        }

        PhotonNetwork.Disconnect();
        PhotonNetwork.LoadLevel(0);
        PhotonNetwork.ConnectUsingSettings();
    }
}
