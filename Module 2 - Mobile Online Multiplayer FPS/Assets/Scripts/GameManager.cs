using System;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour {

    [SerializeField] GameObject playerPrefab;

    [SerializeField] private Transform[] spawnPoints;

    public static GameManager Instance;

    public bool HasWon;

    // Start is called before the first frame update
    void Start() {
        if (Instance == null) {
            Instance = this;
        }
        else if (Instance != this) {
            Destroy(gameObject);
        }

        HasWon = false;

        if (PhotonNetwork.IsConnectedAndReady)
        {
            int randNum = Random.Range(0,5);

            var spawnPosition = new Vector3(spawnPoints[randNum].transform.position.x, 0, spawnPoints[randNum].transform.position.z);

            PhotonNetwork.Instantiate(playerPrefab.name, spawnPosition, Quaternion.identity);
        }
    }

    public void test() {
        Debug.Log("henlo im the manager");
    }

    public Vector3 GetRandomSP() {
        int randNum = Random.Range(0,5);
        var sP = new Vector3(spawnPoints[randNum].transform.position.x, 0, spawnPoints[randNum].transform.position.z);

        return sP;
    }
}
