using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerMovementController : MonoBehaviour {
    
    [SerializeField] public Joystick joystick;

    private RigidbodyFirstPersonController rigidbodyFirstPersonController;

    public FixedTouchField fixedTouchField;

    private Animator animator;
    
    private static readonly int Horizontal = Animator.StringToHash("Horizontal");
    private static readonly int Vertical = Animator.StringToHash("Vertical");
    private static readonly int IsRunning = Animator.StringToHash("IsRunning");

    // Start is called before the first frame update
    void Start() {
        rigidbodyFirstPersonController = GetComponent<RigidbodyFirstPersonController>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate() {
        rigidbodyFirstPersonController.joystickInputAxis.x = joystick.Horizontal;
        rigidbodyFirstPersonController.joystickInputAxis.y = joystick.Vertical;
        rigidbodyFirstPersonController.mouseLook.lookInputAxis = fixedTouchField.TouchDist;

        animator.SetFloat(Horizontal, joystick.Horizontal);
        animator.SetFloat(Vertical, joystick.Vertical);

        if (Mathf.Abs(joystick.Horizontal) > 0.9 || Mathf.Abs(joystick.Vertical) > 0.9) {
            animator.SetBool(IsRunning, true);
            rigidbodyFirstPersonController.movementSettings.ForwardSpeed = 10;
        }
        else {
            animator.SetBool(IsRunning, false);
            rigidbodyFirstPersonController.movementSettings.ForwardSpeed = 5;
        }
    }
}
