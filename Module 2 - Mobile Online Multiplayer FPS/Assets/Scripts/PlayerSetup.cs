using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : MonoBehaviourPunCallbacks {

    [SerializeField] private GameObject fpsModel;
    [SerializeField] private GameObject nonFpsModel;

    public GameObject playerUiPrefab; 

    public PlayerMovementController playerMovementController;

    private Shooting shooting;
    
    public Camera fpsCamera;

    private Animator animator;

    private static readonly int IsLocalPlayer = Animator.StringToHash("IsLocalPlayer");

    public Avatar fpsAvatar;
    public Avatar nonFpsAvatar;

    public Text playerName;

    // Start is called before the first frame update
    private void Awake() {
        playerMovementController = GetComponent<PlayerMovementController>();
        animator = this.GetComponent<Animator>();

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);

        animator.SetBool(IsLocalPlayer, photonView.IsMine);

        animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;

        shooting = this.GetComponent<Shooting>();

        if (photonView.IsMine) {
            GameObject playerUI = Instantiate(playerUiPrefab);
            playerMovementController.fixedTouchField = playerUI.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerUI.transform.Find("Fixed Joystick").GetComponent<FixedJoystick>();
            fpsCamera.enabled = true; 

            playerUI.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shooting.Fire());

            this.photonView.RPC("UpdateName", RpcTarget.AllBufferedViaServer);
        }
        else {
            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false; 
        }
    }

    [PunRPC]
    public void UpdateName() {
        playerName.text = photonView.Owner.NickName;
    }
}
