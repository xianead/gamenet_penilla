﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Shooting : MonoBehaviour
{
    [SerializeField] Camera fpsCamera;

    [SerializeField] public float fireRate = 0.1f;
    private float fireTimer = 0;

    private int dmg = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        if (fireTimer < fireRate) {
            fireTimer += Time.deltaTime;
        }

        if (Input.GetButton("Fire1") && fireTimer >= fireRate) {
            fireTimer = 0.0f; 
            Ray ray = fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f,10f));
            RaycastHit hit;
            
            Debug.DrawLine(this.transform.position, ray.direction, Color.red, Mathf.Infinity);

            if (Physics.Raycast(ray, out hit, 100)) {
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, dmg);
                }
            }
        }
    }
}
