using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks {

    [SerializeField] private Image healthBar;
    
    private float startHealth = 100;
    public float health;
    
    // Start is called before the first frame update
    void Start() {
        health = startHealth;
        UpdateHPBar();
    }

    [PunRPC]
    public void TakeDamage(int damage) {
        health -= damage;
        UpdateHPBar();
        
        if (health < 0) {
            Die();
        }
    }

    private void Die() {
        if (photonView.IsMine) {
            GameManager.Instance.LeaveRoom();
        }
    }

    private void UpdateHPBar() {
        healthBar.fillAmount = health / startHealth;
    }
}
