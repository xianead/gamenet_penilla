using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviourPunCallbacks {

    [SerializeField] private GameObject playerPrefab;

    public static GameManager Instance;

    private void Awake() {
        if (Instance != null) {
            Destroy(this.gameObject);
        }
        else {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start() {
        if (PhotonNetwork.IsConnected) {
            int xRandomPoint = Random.Range(-20, 20);
            int zRandomPoint = Random.Range(-20, 20);
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(xRandomPoint, 0, zRandomPoint),
                Quaternion.identity);
        }
    }

    public override void OnJoinedRoom() {
        Debug.Log(PhotonNetwork.NickName + " has joined the room!");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        Debug.Log(newPlayer.NickName + " has joined the room " + PhotonNetwork.CurrentRoom.Name);
        Debug.Log("Room has now " + PhotonNetwork.CurrentRoom.PlayerCount + "/20 players");
    }

    public override void OnLeftRoom() {
        SceneManager.LoadScene("GameLauncherScene");
    }

    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
    }
}
