using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime; // Handles rooms & lobbies
using Random = UnityEngine.Random;

public class LaunchManager : MonoBehaviourPunCallbacks {

    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;

    private void Awake() {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    // Start is called before the first frame update
    void Start() {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }

    // Called when client is connected to photon servers
    public override void OnConnectedToMaster() {
        Debug.Log(PhotonNetwork.NickName + " connected to photon servers");
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
    }

    // Called when client is connected to the internet. Gets called first before OnConnectedToMaster
    public override void OnConnected() {
        Debug.Log("Connected to internet");
    }

    // Called when there was no room joined. If so, will create a room
    public override void OnJoinRandomFailed(short returnCode, string message) {
        Debug.LogWarning(message);
        CreateAndJoinRandomRoom();
    }

    public void ConnectToPhotonServer() {
        if (!PhotonNetwork.IsConnected) {
            PhotonNetwork.ConnectUsingSettings();
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }
    }

    public void JoinRandomRoom() {
        PhotonNetwork.JoinRandomRoom();
    }

    private void CreateAndJoinRandomRoom() {
        string randomRoomName = "Room " + Random.Range(0, 1000);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
    }

    public override void OnJoinedRoom() {
        Debug.Log(PhotonNetwork.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        Debug.Log(newPlayer.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name + ". Room has now "
        + PhotonNetwork.CurrentRoom.PlayerCount + " players");
    }
}